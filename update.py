#!/usr/bin/python3

# Copyright (C) 2017 Pietro Albini <pietroalbini@ubuntu-it.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import glob
import xml.etree.ElementTree as ET


BANNERS_DIR = os.path.join(os.path.dirname(__file__), "banner")
YEAR_XPATH = ".//*[@id='year']"


def replace(tree, new_year):
    """Replace the year in the """
    for node in tree.findall(YEAR_XPATH):
        node.text = new_year

    return tree


def main(args):
    """Entry point of the script"""
    if len(args) != 1:
        print("Usage: {} <NEW-YEAR>".format(os.path.basename(__file__)))
        print("Example: {} 17".format(os.path.basename(__file__)))
        sys.exit(1)
    new_year = args[0]

    for path in glob.glob("%s/*.svg" % BANNERS_DIR):
        replace(ET.parse(path), new_year).write(path)


if __name__ == "__main__":
    main(sys.argv[1:])
