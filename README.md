# Loghi della DUCC-IT

Questo repository contiene i file .svg dei loghi della DUCC-IT.

## Aggiornare l'anno dei loghi

Il logo in formato banner contiene l'anno dell'edizione corrente. Per
aggiornarlo, eseguire il comando seguente (in questo esempio l'anno è il
`2017`):

```
$ ./update.py 17
```

Dopo l'aggiornamento è gradito un commit nel repository.
